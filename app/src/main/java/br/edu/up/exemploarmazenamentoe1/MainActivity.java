package br.edu.up.exemploarmazenamentoe1;

import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

  EditText txtTexto;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    txtTexto = (EditText) findViewById(R.id.txtTexto);

    SharedPreferences sp = getSharedPreferences("preferencias.txt",0);
    String texto = sp.getString("chave", null);
    if(texto != null){
      txtTexto.setText(texto);
    }
  }

  @Override
  protected void onStop() {
    super.onStop();

    String texto = txtTexto.getText().toString();
    SharedPreferences sp = getSharedPreferences("preferencias.txt", 0);
    SharedPreferences.Editor editor = sp.edit();
    editor.putString("chave", texto);
    editor.commit();
  }


  public void onClickLimpar(View v){
    txtTexto.setText("");
  }

  public void onClickGravarInterno(View v){

    try {
      FileOutputStream fos = openFileOutput("interno.txt", 0);

      String texto = txtTexto.getText().toString();
      fos.write(texto.getBytes());
      fos.close();

      txtTexto.setText("");

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void onClickCarregarInterno(View v){

    try {
      FileInputStream fis = openFileInput("interno.txt");
      byte[] buffer = new byte[fis.available()];
      fis.read(buffer);
      fis.close();
      String texto = new String(buffer);
      txtTexto.setText(texto);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  public void onClickGravarExterno(View v){

    try {
      File sdCard = Environment.getExternalStorageDirectory();
      File arquivo = new File(sdCard, "externo.txt");
      FileOutputStream fos = new FileOutputStream(arquivo);

      String texto = txtTexto.getText().toString();
      fos.write(texto.getBytes());
      fos.close();

      txtTexto.setText("");

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void onClickCarregarExterno(View v){

    try {
      File sdCard = Environment.getExternalStorageDirectory();
      File arquivo = new File(sdCard, "externo.txt");
      FileInputStream fis = new FileInputStream(arquivo);

      byte[] buffer = new byte[fis.available()];
      fis.read(buffer);
      fis.close();
      String texto = new String(buffer);
      txtTexto.setText(texto);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  public void onClickExcluirInterno(View v){

    File pasta = getFilesDir();
    File arquivo = new File(pasta, "interno.txt");
    if (arquivo.exists()){
      arquivo.delete();
    }
  }

  public void onClickExcluirExterno(View v){

    File pasta = Environment.getExternalStorageDirectory();
    File arquivo = new File(pasta, "externo.txt");
    if (arquivo.exists()){
      arquivo.delete();
    }
  }
}











